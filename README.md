# php7-dockerized
Simple PHP7 Docker &amp; Compose Environment 

In order to assist in teaching the building of simple PHP applications and understanding the use of docker and compose this repository was adapted from [Kasper Isager's PHP Dockerized](https://github.com/kasperisager/php-dockerized)

## Technology included

* [Nginx](http://nginx.org/)
* [MySQL](http://www.mysql.com/)
* [PHP 7](http://php.net/)

## Requirements

* [Docker Native](https://www.docker.com/products/overview)

## Running

Clone the repository.
Change directory into the cloned project.
Run the following command.

```sh
$ docker-compose up -d
```

Now the website is available on `http://localhost`.

### Docker environment

The development environment is created with Docker. 
Here's the files and the directories you can operate on to make changes to your development environment.

```
/docker                      
    /nginx                   Files required to build and run nginx (web) container.
        /config              Configuration files for Nginx server.
    /php                     Files required to build and run php container.
        Dockerfile           Dockerfile to build php image.
docker-compose.yml           Definition of the services and the data volumes.   
```  
